package familytree;

import java.util.Random;

public class FamilyTreeTest {

	public static void main(String[] args) {
		PersonManger personManger= new PersonManger();
		personManger.head=testfullfamily();
		
System.out.println(personManger.findmatch("johnt","anna","johnqw","anna"));
	}

	private static Person testfullfamily() {
		Person temp=new Person("john","anna",'M');
		Person temp1=new Person("johnt","anna",'M');
		Person temp2=new Person("johnq","anna",'M');
		Person temp3=new Person("johnw","anna",'M');
		Person temp4=new Person("johne","anna",'M');
		Person temp5=new Person("johnr","anna",'M');
		Person temp6=new Person("johntw","anna",'M');
		Person temp7=new Person("johnwe","anna",'M');
		Person temp8=new Person("johnrw","anna",'M');
		Person temp9=new Person("johnqw","anna",'F');
		Person temp10=new Person("johnfa","anna",'M');
		temp1.setFather(temp);
		temp2.setFather(temp);
		temp3.setFather(temp);
		temp.addKids(temp1);
		temp.addKids(temp2);
		temp.addKids(temp3);
		temp4.setFather(temp1);
		temp5.setFather(temp1);
		temp1.addKids(temp4);
		temp1.addKids(temp5);
		temp6.setFather(temp2);
		temp7.setFather(temp2);
		temp2.addKids(temp6);
		temp2.addKids(temp7);
		temp9.setFather(temp3);
		temp10.setFather(temp3);
		temp3.addKids(temp9);
		temp3.addKids(temp10);
		return temp;
	}

	

}
