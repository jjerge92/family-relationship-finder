package familytree;

import java.util.ArrayList;

public class Person {
	private Person mother,father;
	
	private ArrayList<Person> kids=new  ArrayList<Person>(); 
	private String Partner,  first,  Lastname;
	char gennder;
	private int age;
public Person(String first2, String lastname2, int age2) {
	this.first=first;
	this.Lastname=Lastname;
	this.age=age;
}

public Person(String first,String Lastname, char gennder) {
	this.first=first;
	this.Lastname=Lastname;
	this.gennder=gennder;
}


public ArrayList<Person> getKids() {
	return kids;
}
public void addKids(Person kid) {
	kids.add(kid);
}
public String getPartner() {
	return Partner;
}
public void setPartner(String partner) {
	Partner = partner;
}
public String getFirst() {
	return first;
}
public void setFirst(String first) {
	this.first = first;
}
public String getLastname() {
	return Lastname;
}
public void setLastname(String lastname) {
	Lastname = lastname;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public Person getMother() {
	return mother;
}
public void setMother(Person mother) {
	this.mother = mother;
}
public Person getFather() {
	return father;
}
public void setFather(Person father) {
	this.father = father;
}
public char getGennder() {
	return gennder;
}
public void setGennder(char gennder) {
	this.gennder = gennder;
}
}
